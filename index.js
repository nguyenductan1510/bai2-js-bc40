/**
 * EX1
 */

 function tongLuong() {
    const luong1Ngay = 100000;
    var soNgayLam = document.getElementById("songaylam").value;
  
    var tongLuong = luong1Ngay * soNgayLam;
    console.log("tongLuong", tongLuong);
    document.getElementById("result").innerHTML = 
    
    `<h2 class="mt-5 text-danger text-center">tiền lương của bạn là : </br> ${tongLuong} VNĐ</h2>`
    ;
  }
  
  /**
   * EX2
   */
  
  function trungBinh() {
    var soThuc1 = document.getElementById("sothuc1").value * 1;
    var soThuc2 = document.getElementById("sothuc2").value * 1;
    var soThuc3 = document.getElementById("sothuc3").value * 1;
    var soThuc4 = document.getElementById("sothuc4").value * 1;
    var soThuc5 = document.getElementById("sothuc5").value * 1;
  
    var tinhTrungBinh = Math.floor(
      (soThuc1 + soThuc2 + soThuc3 + soThuc4 + soThuc5) / 5
    );
    document.getElementById("trungBinh").innerHTML = 
    
    `<h2 class="mt-5 text-danger text-center">Trung bình của 5 số thực là : </br> ${tinhTrungBinh} </h2>`
    ;
  }
  
  /**EX3
   */
  
  function thanhTien() {
    const tienVND = 23.5;
    var number = document.getElementById("number").value;
  
    var thanhTien = tienVND * number;
    console.log("Tiền nhận được là", thanhTien);
    document.getElementById("soTienNhan").innerHTML = 
    
    `<h2 class="mt-5 text-danger text-center">Số tiền bạn nhận được là : </br> ${thanhTien} </h2>`
    ;
  }
  
  /**EX4
   */
  function dienTich() {
    var chieuDai = document.getElementById("chieudai").value * 1;
    var chieuRong = document.getElementById("chieurong").value * 1;
  
    var dienTich = chieuDai * chieuRong;
    console.log("Diện tích", dienTich);
    document.getElementById("dienTich").innerHTML = 
    
    `<h2 class="mt-5 text-danger text-center">diện tích hình chữ nhật là : </br> ${dienTich} </h2>`
  }
  function chuVi() {
    var chieuDai = document.getElementById("chieudai").value * 1;
    var chieuRong = document.getElementById("chieurong").value * 1;
    var chuVi = (chieuDai + chieuRong) * 2;
    console.log("Chu vi", chuVi);
    document.getElementById("chuVi").innerHTML = 
    
    `<h2 class="mt-5 text-danger text-center">chu vi hình chữ nhật là : </br> ${chuVi} </h2>`
    ;
  }
  
  /**
   * EX5
   * Đầu vào: Nhận vào 1 số có 2 chữ số
   * Xử lý: lấy ra số đơn vị và số hàng chục từ 1 số có chữ số
   * Đầu ra: tính tổng 2 ký số đã nhận
   */
  
  function tongKySo() {
    var num2 = document.getElementById("num2").value * 1;
  
    var donVi = num2 % 10;
    var hangChuc = Math.floor(num2 / 10);
  
    var result = donVi + hangChuc;
    console.log("Tổng số ký là", result);
    document.getElementById("kySo").innerHTML = 
    
    `<h2 class="mt-5 text-danger text-center">Ký số của 1 số nguyên có 2 chữ số là : </br> ${result} </h2>`
    ;
  }
  